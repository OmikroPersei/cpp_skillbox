 // Helpers.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include <iostream>
#include <string>

class Animal
{
private:
	
public:
	virtual void Voice()
	{
		std::cout << "Animal" << "\n";
	}
};

class Dog : public Animal
{
private:
	
public:
	void Voice() override
	{
		std::cout << "Woof!" << "\n";
	}
};

class Cat : public Animal
{
private:
	
public:
	void Voice() override
	{
		std::cout << "Meow!" << "\n";
	}
};

class Rabbit : public Animal
{
private:
	
public:
	void Voice() override
	{
		std::cout << "Hrum!" << "\n";
	}
};

class Cow : public Animal
{
private:

public:
	void Voice() override
	{
		std::cout << "Moo`!" << "\n";
	}
};

class Kangaroo : public Animal
{
private:

public:
	void Voice() override
	{
		std::cout << "Scream K.O.!" << "\n";
	}
};

class Flea : public Animal
{
private:

public:
	void Voice() override
	{
		std::cout << "Tsok-Tsok-Tsok!" << "\n";
	}
};


int main()
{
	Animal* _Animal[7] = { new Animal, new Dog, new Cat, new Rabbit, new Cow, new Kangaroo, new Flea};
	for (int i = 0; i < 7; i++)
	{
		_Animal[i]->Voice();
	}

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln fi